from setuptools import find_packages, setup

with open("requirements.txt") as f:
    install_requires = f.read().strip().split("\n")

# get version from __version__ variable in master_manager/__init__.py
from master_manager import __version__ as version

setup(
    name="master_manager",
    version=version,
    description="App for managing and syncing masters across multi sites",
    author="Prafful Suthar",
    author_email="prafful@castlecraft.in",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)
