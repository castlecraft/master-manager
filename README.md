## Master Manager

App for managing and syncing masters across multi sites

Use [this](https://gitlab.com/castlecraft/master-manager/-/wikis/home) to setup a local mongoDB and set credentials in adapter to consume it as a virtual DB.

Go to `locatiion_master.py` and update your `host` and `user` `password` for mongodb

#### You can test CRUD on `Location Master` doctype to verify connection

### Creating your own virtual doctype

- Add a doctype with `virtual` check on it
- Go to the respective `your_doctype.py` file

you should see a class like below

```py
class YourDoctype(Document):

    def db_insert(self, *args, **kwargs):
        pass

    def load_from_db(self):
        pass

    ...
```

#### Replace the above class with following and update DB credentials.

```py
import frappe

from master_manager.virtual_master.virtual_mongo_repository import (  # noqa: 501
    VirtualMongoRepository,
)

class YourDoctype(VirtualDynamicRepository):
    # TODO: Update to be from Adapter doctype
    adapter = frappe._dict(
        {
            "name": "mongo_db",
            "database": "mycollection",
            "table_name": "your_doctype",
            "doctype": "Your Doctype",
            "user": "root",
            "password": "changeit",
            "host": "localhost",
        }
    )

    @staticmethod
    def get_list(args):
        return VirtualDynamicRepository.get_list(args, YourDoctype.adapter)

    @staticmethod
    def get_count(args):
        return VirtualDynamicRepository.get_count(args, YourDoctype.adapter)

    @staticmethod
    def get_stats(args):
        return VirtualDynamicRepository.get_stats(args, YourDoctype.adapter)
```

**Once Updated your doctype should be connected with mongoDB and you can do CRUD operations**

#### License

MIT
