import frappe
from pymongo.errors import DuplicateKeyError, ServerSelectionTimeoutError

"""
Following class overwrites all function calls from frappe to Master Manager

Any virtual doctype function calls can be overwritten here

Primary objective to have this is to
- Standardize errors respective to strategy
- Frappe.throw for DB exceptions like mongo DB duplicate error
"""


class BaseMetaWrapper(type):
    def __new__(cls, name, bases, dct):
        for method_name, method in dct.items():
            if callable(method) and method_name != "__init__":
                dct[method_name] = cls.wrap_method(method)
        return super().__new__(cls, name, bases, dct)

    @staticmethod
    def wrap_method(method):
        def wrapper(*args, **kwargs):
            try:
                return method(*args, **kwargs)

            # TODO: Move this to a file with its own strategy
            #       if we endup adding more DBs

            except DuplicateKeyError as e:
                frappe.log_error(
                    "[Master Manager] [RepositoryMeta] DuplicateKeyError", e
                )
                frappe.throw(
                    "Caught a 'DuplicateKeyError', seems like the record/name"
                    " you are trying to create already exists."
                )

            except ServerSelectionTimeoutError as e:
                frappe.log_error(
                    "[Master Manager] [RepositoryMeta] ServerSelectionTimeoutError",  # noqa: 501
                    e,
                )
                frappe.throw(
                    "Faced a timeout when initializing a connection to "
                    "mongoDB, Please check the <b>Virtual Data Adapter</b>"
                    "ensure correct configuration is provided."
                    "<br>"
                    "Please check <b>Error Logs</b> for stack trace."
                )

        return wrapper
