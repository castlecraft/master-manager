import pymongo
import frappe
from frappe.database.utils import DefaultOrderBy
from frappe_utils.frappe_perm_parser.frappe_orm_helper.frappe_sql_helper import (  # noqa: E501 isort: skip
    get_user_permissions_as_hash,
    PermHash,
)

from frappe_utils.frappe_perm_parser.frappe_orm_helper.frappe_query_builder_helper import (  # noqa: E501 isort: skip
    get_linked_doc_references,
)


class MongoFilters:
    filters = []
    permission_filters = []

    def __init__(self, args, is_frappe_list=True) -> None:
        self.doctype_name = args.get("doctype")

        if is_frappe_list:
            self.filters = []
            self.permission_filters = []
            self.append_frappe_list_filters(args.get("filters", []))
            self.set_order_by(args.get("order_by"))
            self.set_limit(args.get("page_length"))
            self.set_skip(args.get("start"))
            self.setup_user_permissions()

    def setup_user_permissions(self):
        """
        Get hash of user permissions
        Example
            <PermHash.all:>: { 'Item Master': ['Cotton'] },
            <PermHash.specific>: {
                'Item Master': [
                    { 'doc': 'Fabric', 'applicable_for': 'Consumer Item', 'is_default': 0 }, # noqa: E501
                    { 'doc': 'Cotton', 'applicable_for': 'Consumer Item', 'is_default': 0 }  # noqa: E501
                ]
            },
            <PermHash.self: 2 >: { 'Item Master': ['Cotton'] } }
        """
        user_permission_hash = get_user_permissions_as_hash(
            frappe.session.user, [self.doctype_name]
        )

        # Map permission applied for all doctypes
        if len(user_permission_hash[PermHash.all]) > 0:
            for key, value in user_permission_hash[PermHash.all].items():

                doctype_references = get_linked_doc_references(
                    key,
                    [self.doctype_name],
                )

                for doc, values in doctype_references.items():
                    self_perm = user_permission_hash[PermHash.self].get(doc)
                    final_value = value if not self_perm else value + self_perm

                    self.append_permission_filters(
                        values.get("fieldname")[0], final_value
                    )

        # Map permissions applied for specific doctypes if link field found
        if len(user_permission_hash[PermHash.specific]) > 0:
            for key, value in user_permission_hash[PermHash.specific].items():

                # Fetch reference doctype with respective technical
                # field name of link
                # {'Linked DocType Name': {'fieldname': ['link_field_name']}}
                doctype_references = get_linked_doc_references(
                    key, [self.doctype_name]
                )  # noqa: E501

                for hash in value:
                    if hash.applicable_for == self.doctype_name:
                        # fmt: off
                        self.append_permission_filters(
                            doctype_references[hash.applicable_for].get("fieldname")[0],  # noqa: E501
                            [hash.doc],
                        )
                        # fmt: onn

        # Map permission applied for self doctype
        if len(user_permission_hash[PermHash.self]) > 0:
            if self.doctype_name in user_permission_hash[PermHash.self]:
                self.append_permission_filters(
                    "name",
                    user_permission_hash[PermHash.self][self.doctype_name],
                )

    def append_frappe_list_filters(self, filters):
        for filter_condition in filters:
            if len(filter_condition) == 3:
                field, operator, value = filter_condition
            elif len(filter_condition) == 4:
                doctype, field, operator, value = filter_condition
            else:
                continue

            self.add_filter(field, operator, value)

    def add_filter(self, field, operator, value):
        filter_dict = {}
        if operator == "like":
            filter_dict[field] = {"$regex": value.replace("%", ".*")}
        if operator == "not like":
            filter_dict[field] = {"$not": {"$regex": value.replace("%", ".*")}}
        elif operator == "in":
            filter_dict[field] = {"$in": value}
        elif operator == "not in":
            filter_dict[field] = {"$nin": value}

        elif operator == "=":
            filter_dict[field] = value
        elif operator == "!=":
            filter_dict[field] = {"$ne": value}
        elif operator == "is" and value == "set":
            filter_dict[field] = {"$ne": None}
        elif operator == "is" and value == "not set":
            filter_dict[field] = None
        elif operator == "<=":
            filter_dict[field] = {"$lte": value}
        elif operator == ">=":
            filter_dict[field] = {"$gte": value}
        elif operator == ">":
            filter_dict[field] = {"$gt": value}
        elif operator == "<":
            filter_dict[field] = {"$lt": value}

        self.filters.append(filter_dict)

    def get_filters(self):
        perms = self.permission_filters or [{}]
        filters = self.filters or [{}]

        return {"$and": [*perms, *filters]}

    def append_permission_filters(self, key, value=None):
        # parse non unique values
        value = list(set(value))

        # If permission_filters is empty, create a new filter
        # with the given key and value
        if not self.permission_filters:
            self.permission_filters = [{key: {"$in": value}}]
            return

        # Check if the key already exists in any filter
        for filter_item in self.permission_filters:
            if key in filter_item:
                # If $in exists check if the value already exists
                # if not append it
                if "$in" in filter_item[key]:
                    for item in value:
                        if item not in filter_item[key]["$in"]:
                            filter_item[key]["$in"].append(item)
                else:
                    filter_item[key]["$in"] = value
                return

        # If key doesn't exist in any filter,
        # create a new filter with the given key and value
        self.permission_filters.append({key: {"$in": value}})

    def set_order_by(self, order_by):
        if not order_by or order_by == DefaultOrderBy:
            self.order_by = [("$natural", pymongo.ASCENDING)]

        else:

            order = order_by.split()[-1]
            field = order_by.split()[-2].split("`")[-2]

            if order == "desc":
                self.order_by = {field: -1}
            elif order == "asc":
                self.order_by = {field: 1}
            else:
                # Default
                self.order_by = {field: 1}

    def get_order_by(self):
        return self.order_by

    def set_skip(self, skip):
        self.skip = int(skip) if skip else 0

    def get_skip(self):
        return self.skip

    def set_limit(self, limit):
        self.limit = 0 if limit is None else (int(limit) or 20)

    def get_limit(self):
        return self.limit
