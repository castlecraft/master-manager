import frappe


"""
    Commits all mongo transactions based on before commit
    hook from `frappe.db.add_before_commit`
"""


def commit_mongo_transactions():
    if not hasattr(frappe.local, "adapter_connections"):
        return

    for name, connection in frappe.local.adapter_connections.items():
        if connection.transaction_started:
            connection.session.commit_transaction()
