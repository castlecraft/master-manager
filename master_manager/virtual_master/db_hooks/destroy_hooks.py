import frappe


def on_distory():
    if not hasattr(frappe.local, "adapter_connections"):
        return

    for name, connection in frappe.local.adapter_connections.items():
        connection.client.client.close()

    print("Mongo connections closed............")  # noqa: 001
