import frappe
from master_manager.modules.utils import (
    is_default_meta_doc,
)
from master_manager.core.master_bootstrap import (
    get_self_site,
)


def get_adapter_config(doctype):
    if is_default_meta_doc(doctype):
        return get_meta_mongo_connection(doctype)

    VDE_doc = frappe.get_cached_doc("Virtual Doctype Extender", doctype)

    if not VDE_doc.get("owner"):
        frappe.throw(
            f"""
                No connection adapter found please set
                Virtual Doctype Extender for {doctype}
                or check configuration.
            """
        )

    connection_adapters = frappe.get_cached_doc(
        "Virtual Data Adapter",
        VDE_doc.adapter,
    )

    if not connection_adapters:
        frappe.throw(f"No adapter found for {VDE_doc.adapter}.")  # noqa: E501

    # Check if Adapter to this doctype owned by self
    if connection_adapters.get("site") == get_self_site().name:
        return build_connection_config(doctype, connection_adapters, True)

    return build_connection_config(doctype, connection_adapters, False)


def get_meta_mongo_connection(doctype):
    connection_adapters = get_connection_adapter("META")

    return build_connection_config(doctype, connection_adapters, True)


def build_connection_config(doctype, connection_adapters, self_owned=False):
    # TODO: Improve this build connection with a strategy.
    user = (
        connection_adapters.get("admin_user")
        if self_owned
        else connection_adapters.get("readonly_user")
    )

    return frappe._dict(
        {
            "name": connection_adapters.get("name"),
            "site": connection_adapters.get("site"),
            "db_type": connection_adapters.get("db_type"),
            "database": connection_adapters.get("database"),
            "replica_set_id": connection_adapters.get("replica_set_id"),
            "cluster": connection_adapters.get("cluster"),
            "user": user.get("user"),
            "password": user.get("password"),
            "doctype": doctype,
            "table_name": frappe.scrub(doctype),
        }
    )


def get_connection_adapter(connection_type="default"):
    connection_adapters = frappe.get_all("Virtual Data Adapter")

    # VDE config not set
    if not connection_adapters:
        frappe.throw(
            "Environment configuration missing for VDE, please provide default connection for VDE to bootstrap virtual doctypes."  # noqa: 504
        )

    adapter = None

    if connection_type == "META":
        adapter = next(
            (
                record
                for record in connection_adapters
                if record["name"] == "META"  # noqa: E501
            ),
            None,
        )
    else:
        adapter = next(
            (
                record
                for record in connection_adapters
                if record["site"] == get_self_site().name
            ),
            None,
        )

    if not adapter:
        frappe.throw(
            "No connection adapter found for requested"
            " connection {}".format(connection_type),
        )

    return adapter
