from master_manager.virtual_master.connections.db_connection import (
    get_adapter_config,
)  # noqa: 501

from master_manager.virtual_master.db_adapters.base_connection_adapter import (  # noqa: 501
    VirtualDBConfigError,
)
from master_manager.virtual_master.db_adapters.mongo_connection_adapter import (  # noqa: 501
    MongoConnectionAdapter,
)

"""
NOTE: Following are supported adapter strategy for
      each DB configured from `virtual data adapter`.

      Any connection resolution and adapter level ACL
      can/should be configured here
"""

SUPPORTED_ADAPTERS = {
    "mongodb": MongoConnectionAdapter,
}


def get_adapter_connection(
    doctype, adapter=None, table=True, as_transaction=False
):  # noqa: 501
    adapter = adapter if adapter else get_adapter_config(doctype)
    if adapter_connection := SUPPORTED_ADAPTERS.get(adapter.db_type)(
        adapter, as_transaction
    ):
        return (
            adapter_connection.get_table()
            if table
            else adapter_connection.get_connection()
        )

    raise VirtualDBConfigError(
        f"No respective strategy found for adapter {adapter.name}."  # noqa: 501
    )
