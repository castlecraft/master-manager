from pymongo import MongoClient, read_concern, WriteConcern

from master_manager.virtual_master.db_adapters.base_connection_adapter import (  # noqa: 501
    BaseConnectionAdapter,
)
import urllib.parse


class MongoConnectionAdapter(BaseConnectionAdapter):
    uri = "mongodb://{user}:{password}@{hosts}/{dbname}?replicaSet={replica_set}"  # noqa: 501

    def init_connection(self):
        cluster = self.adapter.get("cluster", [])
        hosts = [f'{d.get("host")}:{d.get("port")}' for d in cluster]

        uri = self.uri.format(
            user=self.adapter.get("user", "root"),
            #  NOTE: Below is mandatory as passwords may include chars
            #        that are not allowed as a part of URI,
            password=urllib.parse.quote_plus(
                self.adapter.get(
                    "password",
                    "example",
                ),
            ),
            replica_set=self.adapter.get("replica_set_id", "castlecraft"),
            hosts=",".join(hosts),
            dbname=self.adapter.get("database"),
        )

        # Uncomment for loval development for single/non-cluster mongo DB
        # Note: Transactions may not work as expected with single mongo
        # uri = dev_single_mongo_uri(self.adapter)

        connection = MongoClient(uri)

        """
        NOTE: If you're only performing read operations (such as find queries)
              within the session and not making any updates or writes,
              MongoDB won't need to perform any additional bookkeeping or
              locking associated with transactional operations.

              Therefore, the performance impact of creating and using the
              session should be minimal in this case.
        """
        return connection[self.adapter.database], connection.start_session(
            causal_consistency=True
        )

    def get_table(self):
        return self.get_connection()[self.adapter.table_name]

    def close_connection(self):
        return self.get_connection().close()

    def start_transaction(self):
        return self.get_session().start_transaction(
            read_concern=read_concern.ReadConcern("snapshot"),
            write_concern=WriteConcern("majority"),
        )

    def commit_transaction(self):
        return self.get_session().commit_transaction()

    def rollback_transaction(self):
        return self.get_session().abort_transaction()


# For local development
# def dev_single_mongo_uri(adapter):
#     uri = "mongodb://{user}:{password}@{host}:{port}"  # noqa: 501
#     return uri.format(
#         user=adapter.get("user", "root"),
#         password=adapter.get("password", "example"),
#         host=adapter.get("cluster", [])[0].get("host"),
#         port=adapter.get("cluster", [])[0].get("port"),
#     )
