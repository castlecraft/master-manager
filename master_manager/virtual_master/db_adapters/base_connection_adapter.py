import frappe


class VirtualDBConfigError(Exception):
    pass


class BaseConnectionAdapter:
    uri = ("",)

    def __init__(self, adapter, as_transaction=False):
        self.adapter = adapter
        self.connection_name = self.adapter.get("name")

        if not hasattr(frappe.local, "adapter_connections"):
            setattr(frappe.local, "adapter_connections", {})

        if connection := getattr(
            frappe.local.adapter_connections, self.connection_name, None
        ):
            if not as_transaction:
                return connection
            elif connection.transaction_started:
                return connection
            else:
                self.start_transaction()
                connection.transaction_started = True
                return connection

        client, session = self.init_connection()

        frappe.local.adapter_connections[self.connection_name] = frappe._dict(
            {
                "client": client,
                "session": session,
                "transaction_started": as_transaction,
            }
        )

        if as_transaction:
            self.start_transaction()

    def get_connection(self):
        try:
            return frappe.local.adapter_connections[
                self.connection_name
            ].client  # noqa: 501
        except Exception:
            raise VirtualDBConfigError(
                "Invalid configuration, could not get cached client"
                f" initialized for {self.connection_name}"
            )

    def get_session(self):
        try:
            return frappe.local.adapter_connections[
                self.connection_name
            ].session  # noqa: 501
        except Exception:
            raise VirtualDBConfigError(
                "Invalid configuration, could not get cached session"
                f" initialized for {self.connection_name}"
            )

    def close_connection(self):
        connections = frappe.local.adapter_connections[self.connection_name]
        if self.connection_name in connections:
            connections[self.connection_name].client.close()
            del connections[self.connection_name]

    def init_connection(self):
        raise VirtualDBConfigError(
            f"Invalid strategy for {self.adapter.name}, no connection strategy found."  # noqa: 501
        )

    def get_table(self, table_name):
        raise VirtualDBConfigError(
            f"Invalid strategy for {self.adapter.name}, no fetch strategy found."  # noqa: 501
        )

    def start_transaction(self):
        raise VirtualDBConfigError(
            f"Invalid strategy for {self.adapter.name}, no fetch strategy found."  # noqa: 501
        )

    def commit_transaction(self):
        raise VirtualDBConfigError(
            f"Invalid strategy for {self.adapter.name}, no fetch strategy found."  # noqa: 501
        )

    def rollback_transaction(self):
        raise VirtualDBConfigError(
            f"Invalid strategy for {self.adapter.name}, no fetch strategy found."  # noqa: 501
        )
