from frappe.model.document import Document


class BaseFrappeDocument(Document):
    def remove_unpicklable_values(self, state):
        """Remove unpicklable values before pickling"""

        state.pop("mongo", None)
        super(Document, self).remove_unpicklable_values(state)
