# Copyright (c) 2023, Prafful Suthar and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from master_manager.virtual_master.connections.virtual_connection import (
    get_adapter_connection,
)  # noqa: 501
from master_manager.core.masters import convert_list_to_searchable_tuple
from master_manager.virtual_master.db_wrapper.base_meta_wrapper import (
    BaseMetaWrapper,
)
from master_manager.virtual_master.db_wrapper.mongo_filters import MongoFilters
from master_manager.virtual_master.db_strategy.base_frappe_document import (
    BaseFrappeDocument,
)


class MongoRepository(BaseFrappeDocument, metaclass=BaseMetaWrapper):
    def get_mongo(self, as_transaction=False):
        if not hasattr(self, "mongo"):
            self.mongo = get_adapter_connection(
                self.get("doctype"), as_transaction=as_transaction
            )

        return self.mongo

    def db_insert(self, *args, **kwargs):
        d = self.get_valid_dict()
        self.get_mongo().insert_one(d)

    def load_from_db(self):
        d = self.get_mongo().find_one({"name": self.name}) or {}

        doctype = frappe.get_meta(self.doctype)
        for x in doctype.get_table_fields():
            d[x.fieldname] = frappe.db.get_list(
                x.options,
                fields=["*"],
                filters=[["parent", "=", d.get("name")]],
                ignore_permissions=True,
            )
        super(Document, self).__init__(d)

    def db_update(self, *args, **kwargs):
        updated_values = self.get_valid_dict()
        self.get_mongo().update_one(
            {"name": self.name}, {"$set": updated_values}, upsert=True
        )  # noqa: 501

    def delete(self):
        self.get_mongo().delete_one({"name": self.name})

        # delete all records from current doc
        for df in self.meta.get_table_fields():
            self.delete_from_table(df)

        # call after_delete hook here
        # check frappe.model.delete_doc.delete_doc for more

        # self.run_method("after_delete")

    def update_child_table(self, fieldname, df=None):
        """sync child table for given fieldname"""

        # custom logic
        # need improvements

        # if not df:
        #    df = self.meta.get_field(fieldname)

        # self.delete_from_table(df)

        # for row in self.get(fieldname):
        #     row.insert()

        # copied from frappe.model.document.update_child_table
        # and made mongo compatible
        if not df:
            df = self.meta.get_field(fieldname)

        rows = []

        child_db = get_adapter_connection(df.options)

        for d in self.get(df.fieldname):
            d.db_update()
            rows.append(d.name)

        # uncomment when flags validation required
        # if (
        # 	df.options in (self.flags.ignore_children_type or [])
        # 	or frappe.get_meta(df.options).is_virtual == 0
        # ):
        #     # do not delete rows for this because of flags
        # 	# hack for docperm :(
        #     return

        delete_filters = {
            "parent": self.name,
            "parenttype": self.doctype,
            "parentfield": fieldname,
        }

        if rows:
            delete_filters["name"] = {"$nin": rows}

        child_db.delete_many(delete_filters)

    def delete_from_table(self, df):
        """delete all records from current doc"""

        docs = frappe.db.get_list(
            df.options,
            fields=["*"],
            filters=[["parent", "=", self.name]],
            ignore_permissions=True,
        )

        for doc in docs:
            frappe.delete_doc(df.options, doc.get("name"))

    # TODO:
    # need to create mongo utils to convert frappe arguments to mongo filters
    @staticmethod
    def get_list(args):
        db = get_adapter_connection(args.get("doctype"))
        # Release the filter with MongoFIlters from wrapper

        mongo_filters = MongoFilters(args)

        query_result = (
            db.find(mongo_filters.get_filters())
            .skip(mongo_filters.get_skip())
            .limit(mongo_filters.get_limit())
            .sort(mongo_filters.get_order_by())
        )
        data = [{**doc, "_id": str(doc["_id"])} for doc in query_result]

        return (
            convert_list_to_searchable_tuple(data, "name", "name")
            if args.get("as_list")
            else data
        )

    @staticmethod
    def get_count(args):
        db = get_adapter_connection(args.get("doctype"))

        return db.count_documents({})

    @staticmethod
    def get_stats(args):
        # db = get_base_connection(args.get("doctype")
        pass
