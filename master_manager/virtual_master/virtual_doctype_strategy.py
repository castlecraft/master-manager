from enum import Enum
from master_manager.templates.virtual_doctype_template import (
    doctype_virtual_template,
)  # noqa: 501
from master_manager.virtual_master.db_strategy.mongo_repository import (
    MongoRepository,
)  # noqa: 501
from master_manager.modules.utils import camel_case_to_snake_case

"""
Note: Following is strategy for helping when a master is declared
      for the first time.

    Below strategy will be used for overwriding actual doctype classes.

    Configured strategy will be called when doctype operations are referenced
"""


class AddapterTypes(Enum):
    mongodb = "mongodb"


VIRTUAL_STRATEGY = {
    (AddapterTypes.mongodb): {
        "template": doctype_virtual_template,
        "repository": MongoRepository,
        "repository_name": MongoRepository.__name__,
        "repository_file": camel_case_to_snake_case(MongoRepository.__name__),
    },
}


# Get respective strategy to overwrite doctype classes
def get_virtual_strategy(type=AddapterTypes.mongodb):
    return VIRTUAL_STRATEGY[type]
