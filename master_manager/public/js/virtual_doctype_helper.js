/* global frappe */

const MASTER_CONFIRM_MESSAGE = `
<p>Are you sure you want to declare this doctype as <strong>Virtual Master?</strong><p>
This action <span style="color:red">cannot be rolledback or aborted</span>
and will update following.

<br>

<li>Update Doctype controllers</li>
<li>Setup virtual connection with sites default adapter DB</li>
<li>Update Doctype controllers</li>
<li>Create a 'Virtual Doctype Extender'</li>
<li>Export doctype to 'Schema Source'</li>
<li>Make it available to be shared for other sites via 'Frappe Shared Resources'</li>

<br>
<p>Its recommended to consult a <strong>technical person</strong> before performing this action</p>
`;
const SUBSCRIBED_MASTER_INTRO = `
Following doctype is subscribed Master and not a part of your site,
Its only available for readonly purpose and for any sync operation please go to 'Frappe Shared Resources'.
`;
let state = {
  VDE: undefined,
  FRAPPE_SITE: undefined,
};

frappe.ui.form.on('DocType', {
  refresh: async frm => {
    addSyncButton(frm);
    addMigrateDataBtn(frm);
  },
  onload: async frm => {
    onDoctypeLoad(frm);
  },
});

function addMigrateDataBtn(frm) {
  if (!frm.doc.is_virtual) return;

  const excludeModules = [
    'Dev Utils',
    'Frappe Utils',
    'Master Manager',

    // frappe app module
    'Frappe Custom',
    'Automation',
    'Social',
    'Contacts',
    'Printing',
    'Integrations',
    'Desk',
    'Geo',
    'Custom',
    'Email',
    'Workflow',
    'Website',
    'Core',
  ];

  if (excludeModules.includes(frm.doc.module)) return;

  frm.add_custom_button('Migrate Data to Virtual Doctype', () => {
    frappe.call({
      method: 'master_manager.api.migrate_data_to_virtual_doctype',
      args: { doctype: frm.doc.name },
      freeze: true,
      freeze_message: 'Syncing Data',
      callback: r => {
        frappe.show_alert(
          {
            message:
              'Data migration has been pushed to the background. Please check the <b>Error Logs</b> for any errors.',
            indicator: 'green',
          },
          5,
        );
      },
    });
  });
}
function addSyncButton(frm) {
  if (!frm.doc.is_virtual) return;
  const site = getSelfSite();
  const vde = getDoctypeVDE(frm);
  // If no VDE then its a fresh doctype, enable declaration
  if (!vde) {
    addDeclareMasterManagerButton(frm);
  }

  if (site.name !== vde.frappe_site) {
    // If VDE found but not owned by self.site then its a subscribed doctype
    // Make this to be readonly
    // TODO: Below needs to be update so that we can make sure only
    //       disabled fields are enabled
    frm.set_intro(SUBSCRIBED_MASTER_INTRO, 'red');
    frm.toggle_enable('*', 0);
  } else {
    // If VDE exists and is self owned then user can alwaays sync latest schema
    frm.toggle_enable('*', 1);
    addSyncMasterManagerButton(frm);
  }
}

function addDeclareMasterManagerButton(frm) {
  frm.add_custom_button('Declare Master Manager', function () {
    frappe.confirm(MASTER_CONFIRM_MESSAGE, () => {
      frappe.call({
        method: 'master_manager.api.declare_master_manager',
        args: { doctype_name: frm.doc.name },
        freeze: true,
        freeze_message: 'Syncing Master',
        callback: r => {
          frappe.show_alert(
            {
              message: 'Controllers Updated, Schema sync pushed to background.',
              indicator: 'green',
            },
            5,
          );
          onDoctypeLoad(frm);
        },
      });
    });
  });
}

function onDoctypeLoad(frm) {
  state = {};
  getSelfSite();
  getDoctypeVDE(frm);
  frm.refresh_fields();
}

function addSyncMasterManagerButton(frm) {
  frm.add_custom_button('Publish changes to MDM', function () {
    frappe.call({
      method: 'master_manager.api.sync_master_manager',
      args: { doctype_name: frm.doc.name },
      freeze: true,
      freeze_message: 'Syncing Master',
      callback: r => {
        frappe.show_alert(
          {
            message:
              'Synced doctype meta, consumers can now consume latest updates.',
            indicator: 'green',
          },
          5,
        );
      },
    });
  });
}

/* eslint-disable*/
function getSelfSite() {
  if (state.SELF_SITE) {
    return state.SELF_SITE;
  }

  frappe.call({
    method: 'master_manager.api.get_self_site',
    /* eslint-enable */

    callback: function (res) {
      state.SELF_SITE = res.message;
    },
    async: false,
  });

  return state.SELF_SITE;
}

function getDoctypeVDE(frm) {
  if (state.VDE) {
    return state.VDE;
  }

  frappe.call({
    method: 'master_manager.api.get_doctype_vde',
    /* eslint-enable */
    args: {
      doctype_name: frm.docname || frm.name,
    },
    callback: function (res) {
      state.VDE = res.message || undefined;
    },
    async: false,
  });

  return state.VDE;
}
