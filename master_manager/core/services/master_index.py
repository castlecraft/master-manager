import frappe
import pymongo
from pymongo.errors import DuplicateKeyError
from master_manager.virtual_master.connections.virtual_connection import (
    get_adapter_connection,
)

cli_print = print  # noqa: T001, T002, T202


def create_base_indexes(doctype_name):
    db = get_adapter_connection(doctype=doctype_name)
    index_info = db.index_information()
    index_field = "name"

    unique_index_exists = any(
        index.get("unique", False) and index_field in index["key"]
        for index in index_info.values()
    )

    if not unique_index_exists:
        try:
            db.create_index([(index_field, pymongo.ASCENDING)], unique=True)
            cli_print(
                "[Master Manager] [create_base_indexes] Unique index on"
                f" {index_field} field added successfully."
            )

        except DuplicateKeyError as e:
            frappe.log_error("Master Manager DuplicateKeyError", e)
            frappe.throw(
                f"Failed to index {doctype_name} with 'DuplicateKeyError', "
                "Duplicate values found on data level"
            )
    else:
        cli_print(
            "[Master Manager] [create_base_indexes] Skipped indexing, "
            f"unique index on {index_field} field already exists."
        )
