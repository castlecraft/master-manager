import frappe
from master_manager.core.master_bootstrap import (
    is_doctype_susbscribed,
    get_self_site,
)
from master_manager.core.services.vde_service import has_self_owned_VDE


def sync_doctype_meta(doctype):
    # Subscribed doctype should not be syncing meta
    if is_doctype_susbscribed(doctype.name):
        return

    # Only self owned doctypes should be allowed for meta sync
    if not has_self_owned_VDE(doctype.name):
        return

    # Export meta for doctype to Schema Source

    # copied from frappe.core.doctype.data_import.data_import.export_json
    # removed 1st for loop -
    # considering object json instead of array json

    def post_process(out):
        # Note on Tree DocTypes:
        # The tree structure is maintained in the database via the fields "lft"
        # and "rgt". They are automatically set and kept up-to-date. Importing
        # them would destroy any existing tree structure. For this reason they
        # are not exported as well.
        del_keys = (
            "modified_by",
            "creation",
            "modified",
            "owner",
            "idx",
            "lft",
            "rgt",
        )  # noqa: E501
        for key in del_keys:
            if key in out:
                del out[key]
        for k, v in out.items():
            if isinstance(v, list):
                for child in v:
                    for key in del_keys + (
                        "docstatus",
                        "doctype",
                        "modified",
                        "name",
                    ):  # noqa: 501
                        if key in child:
                            del child[key]

    context = get_self_site()

    doc = frappe.get_list(
        "Schema Source",
        filters=[
            ["doctype_name", "=", doctype.name],
            ["frappe_site", "=", context.name],
        ],
    )

    meta_json = doctype
    post_process(meta_json)

    if doc:
        # update existing Schema Source
        schema_source = frappe.get_doc("Schema Source", doc[0].get("name"))
        schema_source.meta_json = meta_json
        schema_source.save()

    else:
        # create new Schema Source
        schema_source = frappe.get_doc(
            {
                "doctype": "Schema Source",
                "doctype_name": doctype.name,
                "frappe_site": context.name,
                "meta_json": meta_json,
            }
        )
        schema_source = schema_source.insert(ignore_permissions=True)
