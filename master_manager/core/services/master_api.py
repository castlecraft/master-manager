import frappe
import json
from frappe import _
from master_manager.master_manager.doctype.virtual_doctype_extender.VDE_service import (  # noqa: 501
    update_VDE_class_with_strategy,
)
from master_manager.core.master_bootstrap import get_self_site


@frappe.whitelist()
def sync_shared_doctype(doc_name, group_name):
    frappe.only_for("System Manager")
    group_doc = validate_if_doctype_vde_owned_by_owner(doc_name, group_name)
    validate_subscriber(group_doc)
    meta_json = get_source_schema(doc_name)
    meta_json["module"] = get_module(group_name, doc_name)

    for field in meta_json["fields"]:
        field["read_only"] = 1

    doctype = frappe.get_doc(meta_json)
    if frappe.db.exists("DocType", doc_name):
        existing_doc = frappe.get_doc("DocType", doc_name).as_dict()
        setattr(doctype, "creation", existing_doc.creation)
        setattr(doctype, "modified", existing_doc.modified)

        frappe.cache().hset("skip_vde_hook", doc_name, True)
        doctype.save()
    else:
        frappe.cache().hset("skip_vde_hook", doc_name, True)
        doctype.insert()

    update_shared_table_subscribed_version(group_name, doc_name)
    update_VDE_class_with_strategy(doc_name)
    return True


def validate_subscriber(group):

    if group.get("group_owner") == get_self_site().name:
        frappe.throw("Only <b>subscriber or consumer</b> can sync doctype.")


def validate_if_doctype_vde_owned_by_owner(doc_name, group_name):

    frappe_site_group = frappe.get_cached_doc(
        "Frappe Shared Resource",
        group_name,
    )

    vde = frappe.get_cached_doc("Virtual Doctype Extender", doc_name)

    group_owner = frappe_site_group.get("group_owner")
    vde_owner = vde.get("frappe_site")

    if group_owner == vde_owner:
        return frappe_site_group

    frappe.throw(f"VDE is not owned by owner {group_owner}")


def get_source_schema(doc_name):
    schema_source = frappe.get_doc("Schema Source", doc_name).as_dict()
    meta_json = json.loads(schema_source.get("meta_json"))
    return meta_json


def get_module(group_name, doc_name):
    frappe_site_group = frappe.get_doc("Frappe Shared Resource", group_name)
    shared_table = frappe_site_group.get("shared_doctype")

    record = next(
        iter(doc for doc in shared_table if doc.doctype_name == doc_name),
        None,
    )

    if not record:
        frappe.throw(_(f"Doctype {doc_name} doesn't exist in shared table."))

    if not record.get("exported_module"):
        frappe.throw(
            _(
                f"""Select <strong>module</strong> for
                    <strong>{doc_name}</strong>
                    before syncing."""
            )
        )

    return record.get("exported_module")


def update_shared_table_subscribed_version(group_name, doc_name):
    frappe_shared_resource = frappe.get_doc(
        "Frappe Shared Resource", group_name
    )  # noqa: 5001

    frappe_shared_resources = frappe.get_all(
        "Frappe Shared Resource",
        filters=[
            ["group_owner", "=", frappe_shared_resource.group_owner],
            ["for_site", "=", frappe_shared_resource.for_site],
        ],
    )

    # considering single owner per doctype
    shared_table = frappe.get_all(
        "Shared Table",
        filters=[
            ["doctype_name", "=", doc_name],
            [
                "parent",
                "in",
                [fsr.get("name") for fsr in frappe_shared_resources],  # noqa: 501
            ],
        ],
    )

    for st in shared_table:
        st = frappe._dict(st)

        # TODO:
        # Use frappe.db.set_value instead
        # once database api is ready for mongodb

        shared_table_doc = frappe.get_doc("Shared Table", st.name)
        shared_table_doc.subscribed_version = (
            shared_table_doc.published_version
        )  # noqa: 501
        shared_table_doc.save()
