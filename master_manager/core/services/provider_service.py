import frappe
import json
from frappe_utils.common.util import sync_json_to_schema


def update_self_schema(doc):
    subscribed_tables = doc.get("subscribed_table")

    for subscription in subscribed_tables:
        meta_list = frappe.get_all(
            "Schema Source",
            filters=[
                ["doctype_name", "=", subscription.get("doctype_name")],
            ],
        )

        if meta_list:
            # TODO: Update this with something like get_last_doc
            #       once we have overwriting class functional
            meta_doc = meta_list[0]
            schema = frappe._dict(json.loads(meta_doc.get("meta_json")))
            sync_json_to_schema(schema)

        else:
            frappe.log_error(
                "No meta found for doctype:"
                " {}".format(subscription.get("doctype_name"))
            )
