import frappe
from master_manager.virtual_master.connections.db_connection import (
    get_connection_adapter,
)  # noqa: 501
from master_manager.core.master_bootstrap import get_self_site


def create_vde_from_doctype(doctype):
    self_site = get_self_site()
    vde_list = frappe.get_list(
        "Virtual Doctype Extender",
        filters=[["virtual_doctype_name", "=", doctype.name]],
    )

    if not vde_list:
        default_VDA = get_connection_adapter()
        vde_doc = frappe.get_doc(
            {
                "doctype": "Virtual Doctype Extender",
                "virtual_doctype_name": doctype.name,
                "adapter": default_VDA.get("name"),
                "frappe_site": self_site.name,
            }
        )
        vde_doc = vde_doc.insert(ignore_permissions=True)


def has_self_owned_VDE(doctype_name):
    vde = frappe.get_doc("Virtual Doctype Extender", doctype_name).as_dict()

    if not vde.owner:
        return False

    if vde.frappe_site != get_self_site().name:
        frappe.throw(
            "Operations cannot be executed on doctypes that "
            "do not have a self owned VDE."
        )

    return True


def remove_VDE(doc):
    if not doc.is_virtual:
        return

    frappe.delete_doc("Virtual Doctype Extender", doc.name)
