import frappe


def get_self_site():
    context = get_site_details()

    provider = next(
        (
            record
            for record in frappe.get_all("Frappe Site")
            if record["site"] == context.get("site_name")
        ),
        None,
    )

    if not provider:
        frappe.throw(
            "No configured provider found for site "
            "{}, please check environment config".format(
                context.get("site_name")
            )  # noqa: E501
        )

    return frappe._dict(provider)


def is_doctye_shared(doctype_name, provider=None):
    # TODO: Update this to be from Frappe Shared Resource

    return False


def is_doctype_susbscribed(doctype_name, provider=None):
    # TODO: Update this to be from Frappe Shared Resource

    return False


def get_master_versions(doctye):
    return True


def get_site_details():
    site_details = {
        "site_name": frappe.local.site,
        "site_url": frappe.utils.get_url(),
        "database_name": frappe.db.db_name,
        "database_user": frappe.db.user,
    }

    return site_details
