import frappe
from master_manager.core.master_bootstrap import (  # noqa: 501 isort: skip
    get_self_site,
)

# TODO: Add a better way to cache what doctypes
#       are not virtual so we can quick skip these
SKIP_VDE_CHECK_FOR_DOCTYPE = [
    "Error Log",
    "Activity Log",
    "Version",
    "Audit Trail",
    "Recorder",
    "File",
    "Version",
    "Communication",
    "DocPerm",
    "RQ Job",
    "User Social Login",
    "Custom Role",
    "Communication Link",
]

EXCLUDE_MODULES = [
    "Workflow",
    "Website",
    "Social",
    "Printing",
    "Integrations",
    "Geo",
    "Email",
    "Desk",
    "Custom",
    "Core",
    "Contacts",
    "Automation",
]


def validate_mdm_track_seen(doc):
    if doc.doctype != "DocType":
        return

    if doc.is_virtual != 1:
        return

    vde = frappe.get_cached_doc(
        "Virtual Doctype Extender",
        doc.name,
    ).as_dict()

    if (
        doc.track_seen == 1
        and doc.module not in EXCLUDE_MODULES
        and vde.owner  # noqa: 501
    ):
        frappe.throw(
            frappe._(
                "`Track Seen` is not supported for MDM doctypes. "
                "Kindly exclude this check to use MDM doctypes bug free."
            )
        )


def validate_master_vde_owner(doc):
    doctype_name = (
        doc.name if doc.get("doctype") == "DocType" else doc.get("doctype")  # noqa: 501
    )

    vde = frappe.get_cached_doc(
        "Virtual Doctype Extender",
        doctype_name,
    ).as_dict()

    if not vde or not vde.owner:
        return

    if vde.frappe_site != get_self_site().name:
        frappe.throw(frappe._("Your are not owner of this doctype."))


def skip_doctype_mdm_checks(doctype_name):
    doctype = frappe.get_meta(doctype_name, True)

    if doctype.get("is_virtual") != 1:
        return True

    # NOTE: Following checks are to ensure a one time schema
    #       update done by consumers via doctype MDM sync or
    #       using `Frappe Shared Resrouce`.
    skip_validation = frappe.cache().hget("skip_vde_hook", doctype_name)

    if skip_validation:
        frappe.cache().hdel("skip_vde_hook", doctype_name)
        return True

    return False
