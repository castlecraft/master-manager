import frappe
from master_manager.core.policies.master_policy import (
    SKIP_VDE_CHECK_FOR_DOCTYPE,
    skip_doctype_mdm_checks,
    validate_mdm_track_seen,
    validate_master_vde_owner,
)


# NOTE: Update this tobe from frappe-utils
def should_skip_hook():
    return (
        frappe.flags.in_install
        or frappe.flags.in_uninstall
        or frappe.flags.in_patch
        or frappe.flags.in_fixtures
        or frappe.flags.in_migrate
    )


def exclude_doc_event_hooks(in_migrate=True):
    def decorator(func):
        def wrapper(self, method=None):
            if in_migrate and should_skip_hook():
                return True

            return func(self, method)

        return wrapper

    return decorator


def validate(self, method=None):
    if should_skip_hook():
        return

    validate_mdm_checks(self)


def validate_mdm_checks(doc):
    doctype_name = doc.name if doc.doctype == "DocType" else doc.doctype  # noqa: 501

    if doctype_name in SKIP_VDE_CHECK_FOR_DOCTYPE:
        return

    if doc.doctype == "DocType" and doc.is_new():
        return

    if skip_doctype_mdm_checks(doctype_name):
        return

    validate_mdm_track_seen(doc)
    validate_master_vde_owner(doc)
