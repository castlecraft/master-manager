import frappe


def before_save(doc, method=None):
    # TODO: Update version on frappe shared resource table
    doc.version = (doc.version + 1) if doc.version else 1
    update_shared_table_published_version(doc)


def update_shared_table_published_version(doc):
    shared_table = frappe.get_all(
        "Shared Table", filters=[["doctype_name", "=", doc.name]]
    )

    for st in shared_table:
        st = frappe._dict(st)

        # TODO:
        # Use frappe.db.set_value instead
        # once database api is ready for mongodb
        shared_table_doc = frappe.get_doc("Shared Table", st.name)
        shared_table_doc.published_version = doc.version
        shared_table_doc.save()

        frappe.get_meta
