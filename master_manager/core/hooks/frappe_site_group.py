from frappe_utils.common.decorators import exclude_doc_event_hooks
from master_manager.core.master_bootstrap import get_self_site


@exclude_doc_event_hooks()
def before_insert(self, method=None):
    set_group_owner(self)


def set_group_owner(doctype):
    self_site = get_self_site()
    doctype.group_owner = self_site.get("name")
