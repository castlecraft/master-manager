doctype_virtual_template = """# Copyright (c) 2023, Prafful Suthar and contributors
# For license information, please see license.txt

from master_manager.virtual_master.db_strategy.{repository_file} import (  # noqa: 501
    {repository_name},
)


class {class_name}({repository_name}):
    pass
"""
