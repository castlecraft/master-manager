# Copyright (c) 2023, Prafful Suthar and contributors
# For license information, please see license.txt

from master_manager.virtual_master.db_strategy.mongo_repository import (  # noqa: 501
    MongoRepository,
)


class LocationMaster(MongoRepository):
    pass
