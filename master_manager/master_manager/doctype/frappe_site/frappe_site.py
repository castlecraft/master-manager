# Copyright (c) 2023, Prafful Suthar and contributors
# For license information, please see license.txt
import frappe
from frappe.model.document import Document
from master_manager.core.masters import convert_list_to_searchable_tuple
import json
from pathlib import Path
from frappe import _

CONFIG_FILE_PATH = "/config/frappe_site.json"


class FrappeSite(Document):
    @staticmethod
    def get_json_file_data():
        adapters_file = frappe.utils.get_bench_path() + CONFIG_FILE_PATH

        data = []

        if Path(adapters_file).is_file():
            with open(adapters_file) as json_data:
                data = json.load(json_data)

        return data

    @property
    def api_secret(self):
        data = FrappeSite.get_json_file_data()
        entry = next((item for item in data if item["name"] == self.name), {})
        return entry.get("api_secret")

    @property
    def api_key(self):
        data = FrappeSite.get_json_file_data()
        entry = next((item for item in data if item["name"] == self.name), {})
        return entry.get("api_key")

    def db_insert(self, *args, **kwargs):
        frappe.throw(_("Insert Not Allowed"))

    def load_from_db(self):
        data = FrappeSite.get_json_file_data()
        entry = next((item for item in data if item["name"] == self.name), {})
        super(Document, self).__init__(entry)

    def db_update(self):
        frappe.throw(_("Update Not Allowed"))

    @staticmethod
    def get_list(args):
        data = FrappeSite.get_json_file_data()

        return (
            convert_list_to_searchable_tuple(data, "name", "site")
            if args.get("as_list")
            else data
        )

    @staticmethod
    def get_count(args):
        return len(FrappeSite.get_json_file_data())

    @staticmethod
    def get_stats(args):
        return {}
