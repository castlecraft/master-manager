/* global frappe */
// Copyright (c) 2024, Prafful Suthar and contributors
// For license information, please see license.txt

const MASTER_META_INDEX_MESSAGE = `
<p>Are you sure you want to index all <b>Master Meta</b> doctypes?<p>
This action <span style="color:red">will index all core Master Meta docs</span>.

<p>
Its recommended to consult a <b>technical person</b> before performing this action,
as this may consume DB resources heavily if there is existing un-indexed data.
</p>
`;

frappe.ui.form.on('Master Manager Settings', {
  refresh(frm) {
    frm.add_custom_button('Index Master Manager Meta Doctypes', function () {
      frappe.confirm(MASTER_META_INDEX_MESSAGE, () => {
        frappe.call({
          method: 'master_manager.api.index_master_manager_meta',
          freeze: true,
          freeze_message: 'Indexing Master',
          callback: r => {
            frappe.show_alert(
              {
                message:
                  'Indexes Updated, Master Meta doctype indexes are synced to latest.',
                indicator: 'green',
              },
              5,
            );
          },
        });
      });
    });
  },
});
