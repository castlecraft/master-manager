// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt
/* global frappe, __ */
let SELF_SITE = false;
frappe.ui.form.on('Frappe Shared Resource', {
  onload: frm => {
    frappe.call({
      method: 'frappe.client.get_list',
      args: { doctype: 'Frappe Site' },
      callback: function (res) {
        const provider = res.message.filter(e => e.site !== getSelfSite().site);
        /* eslint-disable */
        const unique_sites = Array.from(
          new Set(provider.map(item => `${item.name} (${item.site})`)),
        );
        frm.fields_dict.for_site.set_data(unique_sites);
      },
    });

    frappe.call({
      method: 'frappe.client.get_list',
      args: { doctype: 'Module Def' },
      callback: function (res) {
        const modules = res.message.map(x => x.name);

        frm
          .get_field('shared_doctype')
          .grid.update_docfield_property('exported_module', 'options', modules);
      },
    });

    const self_site = getSelfSite();
    if (!self_site) {
      return;
    }
    const name = self_site.name;
    /* eslint-enable */
    frm.set_query('doctype_name', 'shared_doctype', () => {
      return {
        filters: [['frappe_site', '=', name]],
      };
    });
  },

  refresh: function (frm) {
    frm.cscript.sync = (frm, cdt, cdn) => {
      const row = locals[cdt][cdn];

      if (frm.__unsaved) {
        frappe.throw(__('Save doctype before syncing.'));
      }

      frappe.call({
        method: 'master_manager.core.services.master_api.sync_shared_doctype',
        args: {
          doc_name: row.doctype_name,
          group_name: frm.name,
        },
        callback: function (r) {
          if (r.message === true) {
            frappe.msgprint(
              __(
                `DocType <b>${row.doctype_name}</b> Successfully Created and Synced.`,
              ),
            );
          }
        },
      });
    };

    if (!frm.is_new()) {
      frm.add_custom_button(__('Sync All Doctypes'), function () {
        frappe.msgprint(`
          Sync All is not yet implemented,
          please use sync button on table column <b>Sync</b>.
        `);
      });
    }
  },
});

/* eslint-disable*/
function getSelfSite() {
  if (SELF_SITE) {
    return SELF_SITE;
  }
  frappe.call({
    method: 'master_manager.api.get_self_site',
    /* eslint-enable */

    callback: function (res) {
      SELF_SITE = res.message;
    },
    async: false,
  });

  return SELF_SITE;
}

frappe.ui.form.on('Shared Table', {
  doctype_name(frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    frappe.call({
      method: 'master_manager.modules.utils.get_linked_doctypes',
      args: {
        doctype: row.doctype_name,
      },
      type: 'GET',
      callback: function (res) {
        const linkedDoctypes = res.message;

        if (linkedDoctypes.length > 0) {
          frappe.msgprint(
            /* eslint-disable */
            __(
              /* eslint-enable */
              `Please make sure linked doctypes <strong> ${linkedDoctypes} </strong> are also shared to avoid failures and keep consistency.`,
            ),
          );
        }
      },
    });

    // set published version from schema source
    if (row.doctype_name) {
      frappe.db
        .get_doc('Schema Source', row.doctype_name)
        .then(schema_source => {
          frappe.model.set_value(
            row.doctype,
            row.name,
            'published_version',
            schema_source.version,
          );
        });
    }
  },
});
