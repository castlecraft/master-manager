import frappe
from master_manager.virtual_master.virtual_doctype_strategy import (
    get_virtual_strategy,
    AddapterTypes,
)
from master_manager.modules.utils import (
    get_doc_path,
    is_default_meta_doc,
    get_virtual_doctype_class_name,
)
import os


def update_VDE_class_with_strategy(document_name):
    # TODO: Update below from static mongo db to adapter.db_type
    strategy = get_virtual_strategy(AddapterTypes.mongodb)

    template_with_class_adapter = strategy.get("template").format(
        class_name=get_virtual_doctype_class_name(document_name),
        repository_file=get_virtual_doctype_class_name(
            strategy.get("repository_file")
        ),  # noqa: 501
        repository_name=get_virtual_doctype_class_name(
            strategy.get("repository_name")
        ),  # noqa: 501
    )

    file_name = f"{frappe.scrub(document_name)}.py"

    doc = frappe.get_doc("DocType", document_name)
    target_path = get_doc_path(doc.get("module"), document_name)
    target_file_path = os.path.join(target_path, file_name)

    # Clear the file content
    with open(target_file_path, "w") as target_file:
        target_file.write(template_with_class_adapter)


def check_VDE_config_doctypes(document):
    if is_default_meta_doc(document.name):
        frappe.throw(
            "Provided document is a master manager doctype and cannot be a part of VDE.",  # noqa: E501
        )

    pass
