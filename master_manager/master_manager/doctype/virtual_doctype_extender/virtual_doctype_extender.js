// Copyright (c) 2023, Prafful Suthar and contributors
// For license information, please see license.txt
/* global frappe */

frappe.ui.form.on('Virtual Doctype Extender', {
  onload: frm => {
    frm.set_query('virtual_doctype_name', () => {
      return {
        filters: {
          is_virtual: 1,
        },
      };
    });
    fetchAndPopulateAdapterNames(frm);
  },
});

function fetchAndPopulateAdapterNames(frm) {
  frappe.call({
    method: 'frappe.client.get_list',
    args: {
      doctype: 'Virtual Data Adapter',
      fields: ['name'],
    },
    callback: function (r) {
      if (r.message) {
        // Clear existing options
        frm.set_df_property('adapter', 'options', '');

        // Populate options with adapter names
        const adapterNames = r.message.map(doc => doc.name);
        frm.set_df_property('adapter', 'options', adapterNames.join('\n'));
        frm.refresh_field('adapter');
      }
    },
  });
}
