# Copyright (c) 2023, Prafful Suthar and contributors
# For license information, please see license.txt

from master_manager.virtual_master.db_strategy.mongo_repository import (  # noqa: 501
    MongoRepository,
)
from frappe_utils.common.decorators import exclude_doc_event_hooks
from master_manager.master_manager.doctype.virtual_doctype_extender.VDE_service import (  # noqa: 501
    check_VDE_config_doctypes,
)


class VirtualDoctypeExtender(MongoRepository):
    @exclude_doc_event_hooks()
    def validate(self, method=None):
        check_VDE_config_doctypes(self)
