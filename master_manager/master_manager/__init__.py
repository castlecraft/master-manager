import frappe
from frappe import destroy as frappe_distroy

from master_manager.virtual_master.db_hooks.destroy_hooks import (
    on_distory,
)


def overwrite_destroy():
    on_distory()
    frappe_distroy()


# Monkey patch frappe distroy for MDM cache and connection clear
frappe.destroy = overwrite_destroy
# TODO: Add after cluster transaction strategy is resolved
# frappe.db.add_before_commit(commit_mongo_transactions)
