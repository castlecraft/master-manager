import frappe
import os
from re import sub

# Global or shared doctypes that needs to have single state
DEFAULT_META_DOCTYPES = [
    "Frappe Shared Resource",
    "Schema Source",
    "Shared Table",
    "Subscribed Table",
    "Virtual Doctype Extender",
]


def is_default_meta_doc(doc_name):
    return True if doc_name in DEFAULT_META_DOCTYPES else False


def get_doc_path(module, doc_name):
    dn = frappe.scrub(doc_name)
    return os.path.join(frappe.get_module_path(module), "doctype", dn)


def get_virtual_doctype_class_name(doctype):
    return doctype.replace(" ", "")


def camel_case_to_snake_case(s):
    return "_".join(
        sub(
            "([A-Z][a-z]+)",
            r" \1",
            sub("([A-Z]+)", r" \1", s.replace("-", " ")),  # noqa: 501 isort:skip
        ).split()
    ).lower()


@frappe.whitelist()
def get_linked_doctypes(doctype):
    linked_doctypes = set()

    doc_field = frappe.get_all(
        "DocField",
        filters=[["parent", "=", doctype], ["fieldtype", "=", "Link"]],
        fields=["options"],
    )

    # Filtering to get only doctype name (unique)
    linked_doctypes = {item["options"] for item in doc_field}

    # converting into list
    linked_doctypes = list(linked_doctypes)

    return linked_doctypes
