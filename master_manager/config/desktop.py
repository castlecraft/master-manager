from frappe import _


def get_data():
    return [
        {
            "module_name": "Master Manager",
            "type": "module",
            "label": _("Master Manager"),
        }
    ]
