import unittest
import random
import frappe
from master_manager.test_helpers.doctype_test_helper import get_base_doctype
from master_manager.core.master_bootstrap import get_self_frappe_site
from master_manager.api import (
    sync_master_manager,
    sync_subscribed_tables_from_provider,
)
import secrets

# List to store the records and doctypes to be deleted
# Sequence is IMPORTANT
tear_down_data = {
    "User": [],
    "DocType": [],
}

choice_of_docs = [1, 5, 15, 20, 50, 80]


def create_doctype(name, number_of_fields=1):
    doctype = frappe.get_doc(get_base_doctype(number_of_fields))
    doctype.name = name
    doctype.__newname = name
    doctype.insert()
    tear_down_data["DocType"].append(doctype.name)


def create_user(name, email):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": [{"role": "System Manager"}],
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        tear_down_data["User"].append(user_doc.email)


class TestMDMFlow(unittest.TestCase):
    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")

    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def setUp(self):
        # Create (10 * Each Test) doctypes with random number of fields
        for i in range(0, 10):
            no_of_fields = random.choice(choice_of_docs)
            doctype_name = "Master {} Fields {} {}".format(
                i,
                no_of_fields,
                secrets.token_hex(3),
            )
            create_doctype(doctype_name, no_of_fields)
            sync_master_manager(doctype_name)
            sync_subscribed_tables_from_provider()

    def tearDown(self):
        # DB Cleaning goes here....
        frappe.set_user("Administrator")

        # Remove all other doctypes
        for key, array in tear_down_data.items():
            for element in array:
                frappe.delete_doc(key, element)

    def test_schema_source_exported_correctly(self):
        schema_source_list = frappe.get_list("Schema Source")

        for doc_name in tear_down_data["DocType"]:
            vde = next(
                (
                    obj
                    for obj in schema_source_list
                    if obj.get("doctype_name") == doc_name
                ),
                None,
            )

            self.assertIsNotNone(vde, f"Found no Schema Source for {doc_name}")

    def test_VDE_generated_correctly(self):
        vde_list = frappe.get_list("Virtual Doctype Extender")

        for doc_name in tear_down_data["DocType"]:
            vde = next(
                (obj for obj in vde_list if obj.get("name") == doc_name),
                None,
            )

            self.assertIsNotNone(vde, f"Found no VDE for {doc_name}")

    def test_shared_table_set_correctly(self):
        frappe_site_list = frappe.get_list("Frappe Site")
        self.assertGreater(
            len(frappe_site_list),
            0,
            "Found no Site when checking for shared table.",
        )

        self_site = get_self_frappe_site()
        self.assertIsNotNone(self_site, "Found no site configured for self.")

        for doc_name in tear_down_data["DocType"]:
            vde = next(
                (
                    obj
                    for obj in self_site.get("shared_table", [])
                    if obj.get("doctype_name") == doc_name
                ),
                None,
            )

            self.assertIsNotNone(
                vde,
                f"Found no shared table found for {doc_name}",
            )
