import frappe
from master_manager.core.master_bootstrap import get_self_site as get_site
from master_manager.core.services.meta_manager_service import (
    sync_doctype_meta,
)
from master_manager.modules.utils import (
    is_default_meta_doc,
    DEFAULT_META_DOCTYPES,
)
from master_manager.core.masters import (
    get_all_masters,
    get_master_linked_tree,
)
from master_manager.core.services.provider_service import (
    update_self_schema,
)
from master_manager.master_manager.doctype.virtual_doctype_extender.VDE_service import (  # noqa: 501
    update_VDE_class_with_strategy,
)
from master_manager.core.services.vde_service import create_vde_from_doctype
from master_manager.core.services.master_index import create_base_indexes

from master_manager.virtual_master.connections.virtual_connection import (
    get_adapter_connection,
)  # noqa: 501
from pymongo import UpdateOne

"""
    Update to return all doctypes set as master.
    check dev utils master mapping.
"""


@frappe.whitelist(allow_guest=True)
def fetch_all_masters():
    return get_all_masters()


"""
    Following function should return a tree of linked doctype
    Example

    We have `Invoice` `Customer` `Warehouse`
      Invoice =(Links)=> [`Customer` +`Warehouse` ]

      Customer: Flat doctype (no links)
      Warehouse: =(Links)=> [`Location`]
      Location: =(Links)=> [`Address`]
      Address: Flat doctype

    Hence following doctype links should be bootstrapped bottom to top

    Function should return a value like this
    {
        0: ["Invoice"],
        1: ["Customer","Warehouse"]
        2: ["Location"],
        3: ["Address"],
    }
"""


@frappe.whitelist(allow_guest=True)
def fetch_master_linked_tree():
    return get_master_linked_tree()


@frappe.whitelist()
def sync_subscribed_tables_from_provider():
    update_self_schema(get_site())


@frappe.whitelist()
def sync_master_manager(doctype_name):

    if is_default_meta_doc(doctype_name):
        frappe.throw(
            "{} is a META Level "
            "doctype and cannot be updated.".format(doctype_name)  # noqa: E501
        )

    doc = frappe.get_doc("DocType", doctype_name).as_dict()

    if not doc.is_virtual:
        frappe.throw("Only virtual doctypes can be synced for master.")

    # Export latest meta to Schema Source
    sync_doctype_meta(doc)

    # Create base index like uniqueness on `name` to align with frappe
    create_base_indexes(doc.name)


@frappe.whitelist()
def declare_master_manager(doctype_name):
    if is_default_meta_doc(doctype_name):
        frappe.throw(
            "{} is a META Level "
            "doctype and cannot be updated.".format(doctype_name)  # noqa: E501
        )

    doc = frappe.get_doc("DocType", doctype_name).as_dict()

    if not doc.is_virtual:
        frappe.throw("Only virtual doctypes can be declared as master.")

    # Validate doctype to not have track seen
    if doc.track_seen == 1:
        frappe.throw(
            frappe._(
                "`Track Seen` is not supported for MDM doctypes. "
                "Kindly exclude this check to use MDM doctypes bug free."
            )
        )

    # Add a VDE if does not exists
    create_vde_from_doctype(doc)

    # Export latest meta to Schema Source
    sync_doctype_meta(doc)

    # Create base index like uniqueness on `name` to align with frappe
    create_base_indexes(doc.name)

    # Update doctype templates and controller class
    # NOTE: Any code after this will not be executed.
    update_VDE_class_with_strategy(doc.name)


@frappe.whitelist()
def get_self_site():
    frappe.only_for("System Manager")
    frappe_site = get_site()
    return frappe._dict(
        {
            "name": frappe_site.name,
            "site": frappe_site.site,
        }
    )


@frappe.whitelist()
def get_doctype_vde(doctype_name):
    frappe.only_for("System Manager")

    doc = frappe.get_doc("Virtual Doctype Extender", doctype_name).as_dict()

    if doc.owner:
        return doc

    return


@frappe.whitelist()
def index_master_manager_meta():
    frappe.only_for("System Manager")

    for doctype_name in DEFAULT_META_DOCTYPES:
        create_base_indexes(doctype_name)


def get_site_details():
    site_details = {
        "site_name": frappe.local.site,
        "site_url": frappe.utils.get_url(),
        "database_name": frappe.db.db_name,
        "database_user": frappe.db.user,
    }

    return site_details


@frappe.whitelist()
def migrate_data_to_virtual_doctype(doctype):
    validate_non_virtual_doctype(doctype)
    validate_vde(doctype)

    frappe.enqueue(
        migrate_data_to_virtual_doctype_in_queue, doctype=doctype, queue="long"
    )  # noqa: 501


def validate_vde(doctype):
    if not get_doctype_vde(doctype):
        frappe.throw(
            "Virtual Doctype must be declared as a Master Manager to proceed with data migration"  # noqa: 501
        )


def validate_non_virtual_doctype(doctype):
    # check if table exists in mariadb
    query = f"SHOW TABLES LIKE 'tab{doctype}'"
    table = frappe.db.sql(query, as_dict=1)

    if not table:
        frappe.throw(
            f"DocType {doctype} does not exist as a non-virtual doctype"
        )  # noqa: 501


def migrate_data_to_virtual_doctype_in_queue(doctype):
    mongo = get_adapter_connection(doctype)

    # get latest modified object from mongo
    last_update = frappe.get_list(doctype, limit=1, order_by="`modified` desc")
    last_update = frappe._dict(last_update[0]) if last_update else {}

    table = frappe.qb.DocType(doctype)

    batch_size = 2000
    offset = 0

    while True:
        query = (
            frappe.qb.from_(table).select("*").limit(batch_size).offset(offset)
        )  # noqa: 501

        if last_update:
            """
            Get data from mariadb where:
            - Modified date is greater than the latest inserted data in Mongo
            - Name does not match with the latest inserted data in Mongo
            """
            query = query.where(
                (table.modified > last_update.modified)
                & (table.name != last_update.name)
            )

        docs = query.run(as_dict=True)

        if not docs:
            break

        operations = []
        try:
            for doc in docs:
                query = {"name": doc["name"]}
                update = {"$set": doc}
                operations.append(UpdateOne(query, update, upsert=True))

            bulk_write = mongo.bulk_write(operations)

            start_offset = offset
            offset += batch_size

            frappe.log_error(
                title=f"Migrate {doctype} Data ({start_offset} - {offset})",
                message=bulk_write,
            )

        except Exception as e:
            frappe.log_error(e)
